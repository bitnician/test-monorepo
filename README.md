I have added the baseUrl in the `ts.config` file inside the ` packages/ui` workspace, hence in the `packages/ui/index`, i can import the testLog.ts like this `import { testLog } from "base/testLog"`

When I am in the root of the project and execute the `npx ts-node src`, it complains about the path of `testLog`:

```


Cannot find module 'base/testLog' or its corresponding type declarations.

1 import { testLog } from "base/testLog"

```
