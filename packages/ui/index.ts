import { testLog } from "base/testLog"

export function FooComponent() {
  const randomNumber = Math.floor(Math.random() * 5)
  console.log({ randomNumber })

  testLog()
}
